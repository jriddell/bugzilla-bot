#!/usr/bin/env ruby
# frozen_string_literal: true

# SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
# SPDX-FileCopyrightText: 2018 Harald Sitter <sitter@kde.org>

require 'pp'
require 'date'
require 'logger'
require 'time'

require_relative '../lib/sentry'

require_relative '../lib/bugzillabot'
require_relative '../lib/constants'
require_relative '../lib/monkey'
require_relative '../lib/template'

# Super old legacy bugs which used NEEDSINFO as (semi-)closure state.
# Don't disturb them. This is also checked programatically but to save
# processing these well know ones are hardcoded.
BLACK_LIST = [142229, 151886, 171627, 196335, 214336, 219515, 257027, 261034,
              278816, 279471, 317893, 319419, 363242, 384661]

puts Bugzillabot::Connection.new.get('version').body

# FIXME: our bugzilla is too old omg
# p c.get 'whoami'

def bug_reminded?(bug)
  comments = bug.comments_since_status_change
  comment = comments[-1]
  return false unless comment
  return true if comment.creator == BOT_USER
  if comment.creator == 'andrew.crouthamel@kdemail.net' &&
     comment.text.downcase.include?('15 days. Please provide the requested information')
    return true
  end
  false
end

to_remind = []
to_close = []
to_revert = []

logger_output = if Bugzillabot.config.log?
                  "#{Bugzillabot.config.log_directory}/processing.log"
                elsif ENV['AUTOMATIC']
                  File::NULL
                else
                  STDOUT
                end
logger = Logger.new(logger_output, 10, 512 * 1024)

puts 'Processing...'

Bugzillabot::Bug.search(status: 'NEEDSINFO') do |bug|
  next if BLACK_LIST.include?(bug.id)

  if bug.product == 'krita'
    comments = bug.comments_since_status_change
    if comments.any? { |x| x.creator == bug.creator }
      logger.info "Bug #{bug.id} had a comment from reporter -> reverting"
      to_revert << bug
      next
    end
  end

  # * Bugs placed into NEEDSINFO status will received a reminder if the ticket is:
  #  - At least 15 days old
  begin
    delta = (Date.today - bug.changed_status_at.to_date).to_i
    if delta < 15
      logger.info "Bug #{bug.id} has needinfo for less than 15 days (#{delta}) -> skipping"
      next
    end
  end

  # AND
  #  - Has not received any comment within 15 days

  # We'll now walk them again to determine the action for them.
  # - Bugs that had no comment in more than 15 days AND the last comment was from
  #   the bot (i.e. a bug which was already reminded) will be closed
  # - Bugs which had no comment in more than 15 days get a reminder otherwise.
  #   This will make the bot user the user to comment and thus trigger the above
  #   scenario if it goes

  begin
    comments = bug.comments_since_status_change
    last_comment = comments[-1]
    was_reminded = bug_reminded?(bug)
    delta = (Date.today - last_comment.time.to_date).to_i unless comments.empty?
    delta ||= (Date.today - bug.changed_status_at.to_date).to_i
    if delta && delta >= 15 && was_reminded
      logger.info "Bug #{bug.id} had no comment within the last 30 days (#{delta}) -> CLOSE"
      to_close << bug
    elsif delta && delta >= 15
      # Skip super old bugs to not disturb legacy bugs. In the past some
      # projects (in particular valgrind used NEEDSINFO as a closure state).
      if delta >= 365
        logger.info "Bug #{bug.id} super old (#{delta}) -> skipping"
      else
        logger.info "Bug #{bug.id} had a comment within the last 30 days (#{delta}) -> REMIND"
        to_remind << bug
      end
    elsif was_reminded
      logger.info "Bug #{bug.id} was already reminded. Reminder not old enough (#{delta})"
    else # !was_reminded
      # This bug is in needsinfo for more than 15 days but has a fairly recent
      # comment that is not a reminder and is thus considered active.
      logger.info "Bug #{bug.id} does not need actioning. [#{delta}; #{was_reminded}]"
    end
  end
end

2.times { puts }

unless ENV['AUTOMATIC'] # Don't need the messages twice.
  # FIXME: revise config key and derive url from there
  to_remind.each do |bug|
    puts "REMINDING #{BUGZILLA_URL}/show_bug.cgi?id=#{bug.id}"
  end

  to_close.each do |bug|
    puts "CLOSING #{BUGZILLA_URL}/show_bug.cgi?id=#{bug.id}"
  end

  to_revert.each do |bug|
    puts "REVERTING #{BUGZILLA_URL}/show_bug.cgi?id=#{bug.id}"
  end
end

puts "Going to close #{to_close.size} and remind #{to_remind.size} bugs"
loop do
  break if ENV['AUTOMATIC']
  puts 'enter "k" or ctrl-c to abort'
  break if gets.strip == 'k'
end

to_remind.each do |bug|
  puts "REMINDING #{BUGZILLA_URL}/show_bug.cgi?id=#{bug.id}"
  bug.comment(Template.remind)
end

to_close.each do |bug|
  puts "CLOSING #{BUGZILLA_URL}/show_bug.cgi?id=#{bug.id}"
  bug.update(status: 'RESOLVED',
             resolution: 'WORKSFORME',
             comment: { body: Template.close })
end

to_revert.each do |bug|
  puts "REVERTING #{BUGZILLA_URL}/show_bug.cgi?id=#{bug.id}"
  # UNCONFIRMED visually is REPORTED in the webui, but not in the API!
  bug.update(status: 'UNCONFIRMED',
             comment: { body: Template.revert })
end

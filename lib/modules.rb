# frozen_string_literal: true

# SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
# SPDX-FileCopyrightText: 2022 Harald Sitter <sitter@kde.org>

require_relative 'modules/crash'
require_relative 'modules/priority'
require_relative 'modules/versions'
require_relative 'modules/wishlist'

# frozen_string_literal: true

# SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
# SPDX-FileCopyrightText: 2023 Harald Sitter <sitter@kde.org>

require_relative 'test_helper'

require 'modules/versions'

class VersionsTest < Minitest::Test
  def versions_too_old
    {
      'plasma' => {
        'almost_eol' => '5.25',
        'oldest_supported' => '6.0',
        'lts_constraints' => ['~> 5.27.0'],
        'protected' => []
      }
    }
  end

  def versions_nudge
    {
      'plasma' => {
        'almost_eol' => '0.13',
        'oldest_supported' => '5.26',
        'lts_constraints' => ['~> 5.27.0'],
        'protected' => []
      }
    }
  end

  def test_too_old
    stub_request(:get, 'https://localhost/rest/bug?Bugzilla_api_key=testing-api-key&creation_time=2017-05-29T21:00:00%2B00:00&last_change_time=2017-05-29T21:00:00%2B00:00&limit=8&offset=0&product=okular&status=UNCONFIRMED')
      .to_return(status: 200, body: File.read("#{__dir__}/fixtures/283477/get.json"))
    stub_request(:get, 'https://localhost/rest/bug?Bugzilla_api_key=testing-api-key&creation_time=2017-05-29T21:00:00%2B00:00&last_change_time=2017-05-29T21:00:00%2B00:00&limit=8&offset=8&product=okular&status=UNCONFIRMED')
      .to_return(status: 200, body: JSON.generate({ bugs: [], faults: [] }))
    stub_request(:get, 'https://localhost/rest/bug/283477/comment?Bugzilla_api_key=testing-api-key')
      .to_return(status: 200, body: JSON.generate({ bugs: { "283477" => { comments: [] } }, comments: {} }))

    put_request = stub_request(:put, 'https://localhost/rest/bug/283477?Bugzilla_api_key=testing-api-key').with do |x|
      data = JSON.parse(x.body)
      assert_equal('RESOLVED', data['status'])
      true
    end.to_return(status: 200, body: JSON.generate({}))

    Runner::Versions.run(logger: Logger.new($stdout), products: %w[okular], versions: versions_too_old)

    assert_requested(put_request)
  end

  def test_too_old_debian
    stub_request(:get, 'https://localhost/rest/bug?Bugzilla_api_key=testing-api-key&creation_time=2017-05-29T21:00:00%2B00:00&last_change_time=2017-05-29T21:00:00%2B00:00&limit=8&offset=0&product=okular&status=UNCONFIRMED')
      .to_return(status: 200, body: File.read("#{__dir__}/fixtures/330431/get.json"))
    stub_request(:get, 'https://localhost/rest/bug?Bugzilla_api_key=testing-api-key&creation_time=2017-05-29T21:00:00%2B00:00&last_change_time=2017-05-29T21:00:00%2B00:00&limit=8&offset=8&product=okular&status=UNCONFIRMED')
      .to_return(status: 200, body: JSON.generate({ bugs: [], faults: [] }))
    stub_request(:get, 'https://localhost/rest/bug/330431/comment?Bugzilla_api_key=testing-api-key')
      .to_return(status: 200, body: JSON.generate({ bugs: { "330431" => { comments: [] } }, comments: {} }))

    put_request = stub_request(:put, 'https://localhost/rest/bug/330431?Bugzilla_api_key=testing-api-key').with do |x|
      data = JSON.parse(x.body)
      assert_equal('RESOLVED', data['status'])
      assert_includes(data['comment']['body'], 'Debian')
      true
    end.to_return(status: 200, body: JSON.generate({}))

    Runner::Versions.run(logger: Logger.new($stdout), products: %w[okular], versions: versions_too_old)

    assert_requested(put_request)
  end

  def test_nudge
    stub_request(:get, 'https://localhost/rest/bug?Bugzilla_api_key=testing-api-key&creation_time=2017-05-29T21:00:00%2B00:00&last_change_time=2017-05-29T21:00:00%2B00:00&limit=8&offset=0&product=okular&status=UNCONFIRMED')
      .to_return(status: 200, body: File.read("#{__dir__}/fixtures/283477/get.json"))
    stub_request(:get, 'https://localhost/rest/bug?Bugzilla_api_key=testing-api-key&creation_time=2017-05-29T21:00:00%2B00:00&last_change_time=2017-05-29T21:00:00%2B00:00&limit=8&offset=8&product=okular&status=UNCONFIRMED')
      .to_return(status: 200, body: JSON.generate({ bugs: [], faults: [] }))
    stub_request(:get, 'https://localhost/rest/bug/283477/comment?Bugzilla_api_key=testing-api-key')
      .to_return(status: 200, body: JSON.generate({ bugs: { "283477" => { comments: [] } }, comments: {} }))

    put_request = stub_request(:put, 'https://localhost/rest/bug/283477?Bugzilla_api_key=testing-api-key').with do |x|
      data = JSON.parse(x.body)
      assert_equal(data.keys, %w[comment])
      assert_includes(data, 'comment')
      assert_includes(data['comment']['body'], 'Please upgrade to the latest version')
      true
    end.to_return(status: 200, body: JSON.generate({}))

    Runner::Versions.run(logger: Logger.new($stdout), products: %w[okular], versions: versions_nudge)

    assert_requested(put_request)
  end
end

<!--
SPDX-License-Identifier: CC0-1.0
SPDX-FileCopyrightText: 2018 Harald Sitter <sitter@kde.org>
-->

# Bugzillabot

Bugzillabot automatically manages NEEDSINFO bugs.

# Requires

- Ruby 3.0+

# Run Types

By default bugzillabot runs in TESTING mode which means it uses the `testing`
type config. This gets used to validate behavior against a staging
bugzilla instance. If all is good bugzillabot can be run with the environment
variable `PRODUCTION=1` to switch it into production mode.

# Installation

Install necessary Ruby dependencies by either running `bundle install --local`
or else using your distro's package manager to install:

- `rubygem-faraday`
- `rubygem-minitest`
- `rubygem-rake`
- `rubygem-webmock`
- `rubygem-multipart-post`
- `rubygem-concurrent-ruby`
- `rubygem-addressable`
- `rubygem-crack`
- `rubygem-hashdiff`
- `rubygem-public_suffix`
- `rubygem-rexml`

# Configuration

You'll need a Bugzilla API key for either the test server (bugstest.kde.org) or
the production server (bugs.kde.org). To get one, log in and go to Preferences >
API keys > Submit Changes. Make a note of the key.

Configuration can be supplied from the `.config.yaml` inside the git working
directory, or by creating a copy of it or a copy of `test/config.yaml` in
`~/.config/bugzillabot.yaml`. Add your API key for whichever server you want to
use.

# Testing

Validate changes by running `bin/bugzillabot`. If running against a system Ruby
installation, add `GEM_HOME=tmp`.

Add an autotest and make sure it works by running `rake test`.

# For production...

To run against the production server, add `PRODUCTION=1`. This will run against
real bug reports; get in touch with sysadmins about it before you do anything.

It's run through a cron job. Server-side setup gets done via the setup script in
this repo. The crontab file in the repo contains the crontab setup used on the
server.

# Debugging

You can enable HTTP level debugging by setting `DEBUG=1` in the environment.
This does strip tokens, so no access data gets leaked by this.
